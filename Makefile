
PARAMS = -t beamer --variable fontfamily='\ttdefault' -H header.tex main.md
PDF = res.pdf
TEX = main.tex
TMP = *.aux *.log *.nav *.out *.snm *.toc *.vrb

.PHONY: default clean pdf tex

default: pdf

clean:
	rm -f $(TEX) *.pdf $(TMP)

pdf: $(PDF)

$(PDF): main.md header.tex Makefile
	pandoc $(PARAMS) -o $@

tex: $(TEX)

$(TEX): main.md header.tex Makefile
	pandoc $(PARAMS) -o $@

# --variable classoption='usepdftitle=false' \
